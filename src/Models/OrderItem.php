<?php namespace Search\Models;

use Illuminate\Database\Eloquent\Model;
use Search\Models\Revenue;
use Search\Models\SearchClick;


class OrderItem extends Model
{   
    protected $table = 'orderitems';

    protected $fillable = ["phpsessionid", "productid", "qty", "price"];
    

    
    /**
     * Register model boot callbacks
     *
     * @return void
     **/
    public static function boot()
    {
        static::created( function ($orderItem) {
            $productid = $orderItem->productid;
            $sessionid = $orderItem->phpsessionid;

            $searchClick = SearchClick::where('productid', $productid)
                                      ->whereHas('search', function ($q) use ($sessionid) {
                                            $q->where('phpsessionid', $sessionid);
                                      })
                                      ->with('search')->first();

            $phrase = $searchClick->search->phrase;
            $revenue = Revenue::where('phrase', $phrase)->first();
            if ($revenue) {
                $revenue->update(['amount' => $revenue->amount + $orderItem->price, 'clicks' => $revenue->clicks + 1]);
            } else {
                Revenue::create(['phrase' => $phrase, 'amount' => $orderItem->price, 'clicks' => 1]);
            }
        });
    }

}
