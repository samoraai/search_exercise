<?php namespace Search\Models;

use Illuminate\Database\Eloquent\Model;


class Search extends Model
{   
    protected $table = 'search';

    protected $fillable = ["phrase", "dateadded", "numresults", "phpsessionid"];
    
    const CREATED_AT = 'dateadded';


     /**
     * Each search phrase is linked to a click
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function clicks()
    {
        return $this->hasMany('Search\\Models\\SearchClick', 'searchid');
    }
}
