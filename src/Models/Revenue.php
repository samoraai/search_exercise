<?php namespace Search\Models;

use Illuminate\Database\Eloquent\Model;


class Revenue extends Model
{   
    protected $table = 'revenue_by_phrase';

    protected $fillable = ["phrase", "amount", "clicks"];

}
