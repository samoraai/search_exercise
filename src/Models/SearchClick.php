<?php namespace Search\Models;

use Illuminate\Database\Eloquent\Model;


class SearchClick extends Model
{   
    protected $table = 'searchclick';

    protected $fillable = ["searchid", "dateadded", "productid"];
    

    const CREATED_AT = 'dateadded';


     /**
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function search()
    {
        return $this->belongsTo('Search\\Models\\Search', 'searchid');
    }

}
