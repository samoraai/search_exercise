<?php namespace Search;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Contracts\Cache\Repository;

class ReportGenerator
{

    /**
     * 
     *
     * @var Repository
     **/
    protected $cache;

    /**
     * 
     *
     * @var callback
     **/
    protected $whereDateQuery;
    /**
     * 
     *
     * @var integer
     **/
    protected $cacheTime;
    

    /**
     * Constructor
     *
     * @param Repository $cache
     * @param string $dateStart
     * @param string $dateEnd
     * @param integer $cacheTime In minutes
     * @return void
     **/
    public function __construct($dateStart, $dateEnd, Repository $cache, $cacheTime = 60)
    {
        $this->cache = $cache;
        $this->cacheTime = $cacheTime;

        $this->whereDateQuery = function ($q) use ($dateStart, $dateEnd) {
            $q->where("dateadded", ">", $dateStart);
            $q->where("dateadded", "<", $dateEnd);
        };
    }

    /**
     * Flushes the cache
     *
     * @return void
     **/
    public function flushCache()
    {
        $this->cache->forget('with_clicks');
        $this->cache->forget('without_clicks');
        $this->cache->forget('revenue_per_search');
        $this->cache->forget('high_clicks_low_revenue');
    }

    /**
     * Helper function that builds the base query for searches with and without clicks
     *
     * @param integer $limit
     * @return Illuminate\Database\Query\Builder
     **/
    private function searchGroupedByPhrase($limit)
    {
        $q = DB::table('search')
         ->select(DB::raw('phrase, count(*) as amount'))
         ->where($this->whereDateQuery)
         ->groupBy('phrase')
         ->orderBy('amount', 'DESC');

         if (! is_null($limit)) {
             $q->limit($limit);
         }

         return $q;
    }


    /**
     * Helper function that builds the subquery for searches with and without clicks
     *
     * @param string
     * @return callback
     **/
    private function getClickSubquery()
    {
        return function ($q) {
                    $q->select(DB::raw(1))
                      ->from('searchclick')
                      ->whereRaw('searchclick.searchid = search.id');
                };
    }


    /**
     *
     * @param integer $limit
     * @return Illuminate\Database\Query\Builder
     **/
    private function _getPopularSearchesWithClicksQuery($limit = null)
    {
        $subquery = $this->getClickSubquery();

        return $this->searchGroupedByPhrase($limit)->whereExists($subquery);
    }

    /**
     *
     * @param integer $limit
     * @return Illuminate\Database\Query\Builder
     **/
    private function _getPopularSearchesWithoutClicksQuery($limit = null)
    {
        $subquery = $this->getClickSubquery();

        return $this->searchGroupedByPhrase($limit)->whereNotExists($subquery);
    }

    /**
     * To show the most popular search terms that did produce clicks
     *
     * @param integer $limit
     * @return Illuminate\Support\Collection
     **/
    public function popularSearchesWithClicks($limit = null)
    {
        $query = $this->_getPopularSearchesWithClicksQuery($limit);

        return $this->cache->remember('with_clicks', $this->cacheTime, function () use ($query) {
            return $query->get();
        });
    }

    /**
     * To show the most popular search terms that did not produce clicks
     *
     * @param integer $limit
     * @return Illuminate\Support\Collection
     **/
    public function popularSearchesWithoutClicks($limit = null)
    {
        $query = $this->_getPopularSearchesWithoutClicksQuery($limit);

        return $this->cache->remember('without_clicks', $this->cacheTime, function () use ($query) {
            return $query->get();
        });
    }


    /**
     * Revenue per search term
     *
     * @param integer $limit
     * @return Illuminate\Support\Collection
     **/
    public function revenuePerSearchTerm($limit = null)
    {

        $query = DB::table('search')
                ->select(DB::raw('phrase,count(*),sum(price) as revenue'))
                ->leftJoin('orderitems', 'search.phpsessionid', '=', 'orderitems.phpsessionid')
                ->where($this->whereDateQuery)
                ->groupBy('phrase')
                ->orderBy('revenue', 'DESC');
        
        if (! is_null($limit)) {
            $query->limit($limit);
        }

        return $this->cache->remember('revenue_per_search', $this->cacheTime, function () use ($query) {
            return $query->get();
        });
    }


    /**
     * search phrases with high clicks and low revenue per search
     *
     * @param integer $limit
     * @return Illuminate\Support\Collection
     **/
    public function highClicksLowRevenue($limit = null)
    {

        $popular = $this->popularSearchesWithClicks($limit);


        return $this->cache->remember('high_clicks_low_revenue', $this->cacheTime, function () use ($popular) {

            $revenues = DB::table('revenue_by_phrase')
                ->whereIn("phrase", $popular->pluck('phrase'))
                ->get()->keyBy("phrase");

            foreach ($popular as $key => $clicked) {
                $amount = isset($revenues[$clicked->phrase]) ? $revenues[$clicked->phrase]->amount : 0.0;
                $popular[$key]->clicks = $popular[$key]->amount;
                $popular[$key]->amount = $amount;
            }

            $popular = $popular->sort(function ($item1, $item2) {
                if ($item1->amount == $item2->amount) {
                    return 0;
                }

                return $item1->amount < $item2->amount ? -1 : 1;
            });

            return $popular;
        });
    }


}
