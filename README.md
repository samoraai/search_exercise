# README #
This task is to provide a solution as to how to effectively work with these search tables to produce reports along the lines of "popular search phrases with no clicks", "search phrases with high clicks and low revenue per search"

### Installation ###

* clone the repository
* cd search_exercise
* composer install
* create a MySQL database, and provide connection details in the bootstrap.php file
* To initialise and seed a db: php seed_db.php (I created my MySQL db in a vagrant VM, so the script should run from within the VM as well)

### To run ###

* php index.php
* the available command line options are:
* --limit: limit the queries
* --date-start: the date range start to query over
* --date-end: the date range start to query over
* --noclicks: Most popular search terms that did not produce clicks
* --yesclicks: Most popular search terms that did produce clicks
* --revenue: Revenue per search term
* --hi-clicks-lo-revenue: Search phrases with high clicks and low revenue per search