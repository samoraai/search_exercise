<?php
require_once 'bootstrap.php';

use Search\Models\Search;
use Search\Models\OrderItem;
use Search\Models\SearchClick;
use Illuminate\Database\Schema\Builder as SchemaBuilder;
use Illuminate\Database\Capsule\Manager;

$faker = Faker\Factory::create();
$schema = new SchemaBuilder(Manager::connection());

//$uniquePhrases = 50000;
//$numberPhrases = 1000000;
//$numberProducts = 5000;

$uniquePhrases = 5000;
$numberPhrases = 100000;
$numberProducts = 500;


/************************************************************************
    CREATE TABLES
 ************************************************************************/

$schema->dropIfExists('search');
$schema->create('search', function($table)
{
    $table->bigIncrements('id')->unsigned();
    $table->string('phrase')->index();
    $table->dateTime('dateadded');
    $table->integer('numresults')->unsigned();
    $table->string('phpsessionid');
    $table->dateTime('updated_at');

});


$schema->dropIfExists('searchclick');
$schema->create('searchclick', function($table)
{
    $table->bigIncrements('id')->unsigned();
    $table->bigInteger('searchid')->index();
    $table->dateTime('dateadded');
    $table->integer('productid')->unsigned();
    $table->dateTime('updated_at');

});


$schema->dropIfExists('orderitems');
$schema->create('orderitems', function($table)
{
    $table->bigIncrements('id')->unsigned();
    $table->string('phpsessionid')->index();
    $table->integer('productid')->unsigned()->index();
    $table->smallInteger('qty');
    $table->float('price');
    $table->timestamps();
});

$schema->dropIfExists('revenue_by_phrase');
$schema->create('revenue_by_phrase', function($table) {
    $table->bigIncrements('id')->unsigned();
    $table->string('phrase')->unique();
    $table->float('amount');
    $table->integer('clicks')->default(0);
    $table->timestamps();
});




$phrases = [];
for ($i=0; $i <= $uniquePhrases; $i++) { 
    $phrases[] = $faker->words(3,true);
}

$products = [];
for ($i=0; $i <= $numberProducts; $i++) { 
    $products[] = [
        'id' => $i+1,
        'price' => $faker->randomFloat(2, 10, 2000),
    ];
}



$phpsessionid = $faker->md5();
for ($i=0; $i < $numberPhrases; $i++) {

    //same person might do multiple searches within one session
    $phpsessionid = rand(0,3) > 0 ? $faker->md5() : $phpsessionid;
    $search = Search::create([
        'phrase' => $phrases[rand(0, $uniquePhrases)],
        'numresults' => $faker->numberBetween(50, 100000),
        'phpsessionid' => $phpsessionid,
    ]);

    //randomly create a click for search
    if (rand(0,1)) {
        $product = $products[rand(0, $numberProducts-1)];
        SearchClick::create([
            'searchid' => $search->getAttribute('id'),
            'productid' => $product['id']
        ]);

        //let's just say roughly 1/10 clicks produce an order
        if (rand(0,10) == 0) {
            OrderItem::create([
                'phpsessionid' => $phpsessionid,
                'productid' => $product['id'],
                'qty' => rand(1, 3),
                'price' => $product['price'],
            ]);
        }
    }
}