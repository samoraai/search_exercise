<?php

require_once("./vendor/autoload.php");

use Illuminate\Events\Dispatcher;
use Illuminate\Database\Schema\Grammars\MySqlGrammar;
use Illuminate\Database\Capsule\Manager;

$capsule = new Manager;
$capsule->addConnection([
                'driver'    => 'mysql',
                'host'      => '127.0.0.1',
                'database'  => 'search_exercise',
                'username'  => 'homestead',
                'password'  => 'secret',
                'charset'   => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix'    => ''
            ]);


$capsule->getConnection()->setSchemaGrammar(new MySqlGrammar);
$capsule->setEventDispatcher(new Dispatcher($capsule->getContainer()));

$capsule->setAsGlobal();

$capsule->bootEloquent();