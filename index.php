<?php
require_once("./bootstrap.php");

use Search\Models\Search;
use Search\ReportGenerator;


$limitArg = array_search('--limit', $argv);
$limit = $limitArg ? $argv[$limitArg + 1] : 50;

$dateStartArg = array_search('--date-start', $argv);
$dateStart = $dateStartArg ? $argv[$dateStartArg + 1] : "2016-01-01";

$dateEndArg = array_search('--date-end', $argv);
$dateEnd = $dateEndArg ? $argv[$dateEndArg + 1] : "2016-12-31";




$filestore = new Illuminate\Cache\FileStore(
    new Illuminate\Filesystem\Filesystem,
    dirname(__FILE__) . DIRECTORY_SEPARATOR . "_cache" . DIRECTORY_SEPARATOR
);

$cache = new Illuminate\Cache\Repository($filestore);

$generator = new ReportGenerator($dateStart, $dateEnd, $cache);

(new Search)->getConnection()->enableQueryLog();





if (in_array('--flush-cache', $argv)) {
    $generator->flushCache();
}



function printQueryLog() {
    $hr = "---------------------------------------------------------------\n";
    $format = "%s | %8.8s ms |\n";
    echo $hr;
    echo $hr;
    printf($format, 'Query', 'Time');
    echo $hr;
    foreach ((new Search)->getConnection()->getQueryLog() as $key => $query) {
        printf($format, $query['query'], $query['time']);
    }

    echo $hr;
    echo $hr;
}

$hr = "---------------------------------------------------------------\n";
$format = "%-50.50s | %8.8s |\n";

if (in_array('--yesclicks', $argv)) {
    echo $hr;
    echo "Most popular search terms that did produce clicks\n";
    echo $hr;
    printf($format, 'Phrase', 'Amount');
    echo $hr;
    foreach ($generator->popularSearchesWithClicks($limit) as $key => $row) {
        printf($format, $row->phrase, $row->amount);
    }
}


if (in_array('--noclicks', $argv)) {
    echo $hr;
    echo "Most popular search terms that did not produce clicks\n";
    echo $hr;
    printf($format, 'Phrase', 'Amount');
    echo $hr;
    foreach ($generator->popularSearchesWithoutClicks($limit) as $key => $row) {
        printf($format, $row->phrase, $row->amount);
    }
}

if (in_array('--revenue', $argv)) {
    echo $hr;
    echo "Revenue per search term\n";
    echo $hr;
    printf($format, 'Phrase', 'Revenue');
    echo $hr;
    foreach ($generator->revenuePerSearchTerm($limit) as $key => $row) {
        printf($format, $row->phrase, $row->revenue);
    }
}

if (in_array('--hi-clicks-lo-revenue', $argv)) {
    echo $hr;
    $format = "%-50.50s | %8.8s | %s |\n";
    echo "Search phrases with high clicks and low revenue per search\n";
    echo $hr;
    printf($format, 'Phrase', 'Revenue', 'Clicks');
    echo $hr;
    foreach ($generator->highClicksLowRevenue($limit) as $key => $row) {
        printf($format, $row->phrase, number_format($row->amount, 2, '.', ' '), $row->clicks);
    }
}



printQueryLog();



